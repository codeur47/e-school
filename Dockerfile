FROM openjdk:8-jre-alpine

VOLUME /tmp

ARG DOCKERIZE_VERSION
ARG ARTIFACT_NAME
ARG EXPOSED_PORT
ARG BUILD_DIR=build

ENV SPRING_PROFILES_ACTIVE docker
ENV DOCKERIZE_VERSION v0.6.1
ENV ARTIFACT_NAME e-school-0.0.1-SNAPSHOT


ADD https://github.com/jwilder/dockerize/releases/download/${DOCKERIZE_VERSION}/dockerize-alpine-linux-amd64-${DOCKERIZE_VERSION}.tar.gz dockerize.tar.gz
RUN tar xzf dockerize.tar.gz
RUN chmod +x dockerize

ADD /target/${ARTIFACT_NAME}.jar app.jar

RUN touch /app.jar

EXPOSE ${EXPOSED_PORT}

ENTRYPOINT ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]