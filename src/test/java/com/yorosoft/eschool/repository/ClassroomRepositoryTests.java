package com.yorosoft.eschool.repository;

import static org.assertj.core.api.Assertions.*;
import com.yorosoft.eschool.model.Classroom;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@DataJpaTest
@TestMethodOrder(OrderAnnotation.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ClassroomRepositoryTests {

    @Autowired
    private ClassroomRepository classroomRepository;

    public List<Classroom> initData(){

        Classroom classroom1 = new Classroom();
        classroom1.setClassroomName("Cour Preparatoire 1ere Annee");
        classroom1.setAbbreviation("CP1");
        classroom1.setClassroomStudentsCount(50);

        Classroom classroom2 = new Classroom();
        classroom2.setClassroomName("Cour Preparatoire 2eme Annee");
        classroom2.setAbbreviation("CP2");
        classroom2.setClassroomStudentsCount(50);

        Classroom classroom3 = new Classroom();
        classroom3.setClassroomName("Cour Elementaire 1ere Annee");
        classroom3.setAbbreviation("CE1");
        classroom3.setClassroomStudentsCount(50);

        Classroom classroom4 = new Classroom();
        classroom4.setClassroomName("Cour Elementaire 2eme Annee");
        classroom4.setAbbreviation("CE2");
        classroom4.setClassroomStudentsCount(50);

        return Arrays.asList(classroom1,classroom2,classroom3,classroom4);
    }


    @Test
    @Order(1)
    public void testCreateClassroom(){

        List<Classroom> classroomList = initData();

        classroomList.forEach(classroom -> {
            classroomRepository.save(classroom);
        });

        Classroom classroomCreated = classroomRepository.findByClassroomName("Cour Preparatoire 2eme Annee");

        assertThat(classroomCreated).isNotNull();
        assertThat(classroomCreated.getId()).isGreaterThan(0);
        assertThat(classroomCreated.getAbbreviation()).isNotEmpty();
        assertThat(classroomCreated.getClassroomName()).isEqualToIgnoringCase("Cour Preparatoire 2eme Annee");
        assertThat(classroomCreated.getAbbreviation()).isNotEqualToIgnoringCase("CP1");
    }

    @Test
    @Order(2)
    public void testListClassroom(){

        List<Classroom> classroomList = initData();

        classroomList.forEach(classroom -> {
            classroomRepository.save(classroom);
        });

        List<Classroom> classroomListCreated = classroomRepository.findAll();

        assertThat(classroomListCreated.size()).isGreaterThan(0);
        assertThat(classroomListCreated.size()).isEqualTo(4);


    }

    @Test
    @Order(3)
    public void testUpdateClassroom(){

        List<Classroom> classroomList = initData();

        classroomList.forEach(classroom -> {
            classroomRepository.save(classroom);
        });

        Classroom classroomCreated = classroomRepository.findByClassroomName("Cour Preparatoire 2eme Annee");

        Long oldClassroomId = classroomCreated.getId();
        String oldClassroomName = classroomCreated.getClassroomName();
        String oldClassAbbreviation = classroomCreated.getAbbreviation();
        Integer oldClassroomNumberStudent = classroomCreated.getClassroomStudentsCount();

        classroomCreated.setClassroomName("Cour Preparatoire 2emee Anneee");
        classroomCreated.setAbbreviation("CP22");
        classroomCreated.setClassroomStudentsCount(70);

        Classroom classroomUpdated = classroomRepository.save(classroomCreated);

        assertThat(classroomUpdated.getId()).isEqualTo(oldClassroomId);
        assertThat(classroomUpdated.getClassroomName())
                .isEqualToIgnoringCase("Cour Preparatoire 2emee Anneee")
                .isNotEqualToIgnoringCase(oldClassroomName);

        assertThat(classroomUpdated.getAbbreviation())
                .isEqualToIgnoringCase("CP22")
                .isNotEqualToIgnoringCase(oldClassAbbreviation);

        assertThat(classroomUpdated.getClassroomStudentsCount())
                .isGreaterThan(50)
                .isBetween(50,80)
                .isNotEqualTo(oldClassroomNumberStudent)
                .isNotNull();
    }

    @Test
    @Order(4)
    public void testDeleteClassroom() {

        List<Classroom> classroomList = initData();

        classroomList.forEach(classroom -> {
            classroomRepository.save(classroom);
        });

        Classroom classroomCreated = classroomRepository.findByClassroomName("Cour Preparatoire 2eme Annee");

        classroomRepository.delete(classroomCreated);

        List<Classroom> classroomListUpdated = classroomRepository.findAll();

        assertThat(classroomListUpdated.size())
                .isEqualTo(3);

        Classroom classroomDeleted = classroomRepository.findByClassroomName("Cour Preparatoire 2eme Annee");

        assertThat(classroomDeleted)
                .isNull();

    }
}
