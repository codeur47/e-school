package com.yorosoft.eschool.repository;

import com.yorosoft.eschool.model.Classroom;
import com.yorosoft.eschool.model.Student;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@TestMethodOrder(OrderAnnotation.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class StudentRepositoryTests {

    @Autowired
    private ClassroomRepository classroomRepository;

    @Autowired
    private StudentRepository studentRepository;

    public List<Classroom> initClassroomData(){

        Classroom classroom1 = new Classroom();
        classroom1.setClassroomName("Cour Preparatoire 1ere Annee");
        classroom1.setAbbreviation("CP1");
        classroom1.setClassroomStudentsCount(50);

        Classroom classroom2 = new Classroom();
        classroom2.setClassroomName("Cour Preparatoire 2eme Annee");
        classroom2.setAbbreviation("CP2");
        classroom2.setClassroomStudentsCount(50);

        Classroom classroom3 = new Classroom();
        classroom3.setClassroomName("Cour Elementaire 1ere Annee");
        classroom3.setAbbreviation("CE1");
        classroom3.setClassroomStudentsCount(50);

        Classroom classroom4 = new Classroom();
        classroom4.setClassroomName("Cour Elementaire 2eme Annee");
        classroom4.setAbbreviation("CE2");
        classroom4.setClassroomStudentsCount(50);

        return Arrays.asList(classroom1,classroom2,classroom3,classroom4);

    }

    public List<Student> initStudentData(){

        Student student = new Student();
        student.setLname("YORO");
        student.setFname("Ange Carmel");
        student.setDateOfBirth("03/10/1994");

        Student student1 = new Student();
        student1.setLname("NOUGBELE");
        student1.setFname("Daniel Jonatan");
        student1.setDateOfBirth("13/09/1994");

        Student student2 = new Student();
        student2.setLname("ELIDJE");
        student2.setFname("Aka Emmanuel");
        student2.setDateOfBirth("02/01/1992");

        return Arrays.asList(student,student1,student2);

    }


    @Test
    @Order(1)
    public void testCreateStudent(){

        List<Classroom> classroomList = initClassroomData();
        List<Student> studentList = initStudentData();

        classroomList.forEach(classroom -> {
            classroomRepository.save(classroom);
        });

        Classroom classroomCreated = classroomRepository.findByClassroomName("Cour Preparatoire 2eme Annee");

        studentList.forEach(student -> {
            student.setClassroom(classroomCreated);
            studentRepository.save(student);
        });

        Student studentCreated = studentRepository.findByLnameAndFname("YORO","Ange Carmel");

        assertThat(studentCreated).isNotNull();
        assertThat(studentCreated.getId()).isGreaterThan(0);
        assertThat(studentCreated.getLname()).isEqualToIgnoringCase("YORO");
        assertThat(studentCreated.getFname()).isEqualToIgnoringCase("Ange Carmel");
        assertThat(studentCreated.getFname()).isNotEqualToIgnoringCase("Ange Carmel 1");
    }

    @Test
    @Order(2)
    public void testListClassroom(){

        List<Classroom> classroomList = initClassroomData();
        List<Student> studentList = initStudentData();

        classroomList.forEach(classroom -> {
            classroomRepository.save(classroom);
        });

        Classroom classroomCreated = classroomRepository.findByClassroomName("Cour Preparatoire 2eme Annee");

        studentList.forEach(student -> {
            student.setClassroom(classroomCreated);
            studentRepository.save(student);
        });

        List<Student> studentListCreated = studentRepository.findAll();
        List<String> lnameList = new ArrayList<>();

        studentListCreated.forEach(student -> {
            lnameList.add(student.getLname());
        });

        assertThat(studentListCreated.size()).isNotNull();
        assertThat(studentListCreated.size()).isBetween(0,4);
        assertThat(studentListCreated.size()).isEqualTo(3);
        assertThat(studentListCreated.size()).isGreaterThan(0);
        assertThat(studentListCreated.size()).isLessThan(5);

        studentList.forEach(student -> {
            assertThat(student.getLname()).isIn(lnameList);
        });

    }

}
