package com.yorosoft.eschool.utility;

public abstract class Constant {

    private Constant() {
        throw new IllegalStateException("Utility class");
    }

    public static final String SUCCESS = "success";
    public static final String UPDATE = "update";
    public static final String ERROR = "error";
    public static final String EFFECTIF_ERROR = "effectif_error";

    public static final String DASHBOARD_VIEW = "dashboard";
    public static final String ADDCLASSROOM_VIEW = "addClassroom";
    public static final String LISTCLASSROOM_VIEW = "listClassroom";

    public static final String CLASSROOM_OBJECT = "classroom";
    public static final String CLASSROOM_OBJECT_LIST = "listClassroom";

    public static final String ADD_STUDENT_VIEW = "addStudent";
    public static final String LIST_STUDENT_VIEW = "listStudent";
    public static final String LIST_STUDENT = "listStudent";
    public static final String STUDENT_OBJET = "student";

    public static final String EDIT_MODE = "edit";
    public static final String CREATE_MODE = "create";

}
