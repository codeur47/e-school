package com.yorosoft.eschool.controller;

import com.yorosoft.eschool.model.Classroom;
import com.yorosoft.eschool.model.Student;
import com.yorosoft.eschool.service.ClassroomService;
import com.yorosoft.eschool.service.StudentService;
import com.yorosoft.eschool.utility.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ClassroomController {

    private final ClassroomService classroomService;
    private final StudentService studentService;

    @Autowired
    public ClassroomController(ClassroomService classroomService, StudentService studentService) {
        this.classroomService = classroomService;
        this.studentService = studentService;
    }

    @GetMapping("/addClassroom")
    public String addClassroom(Model model){
        model.addAttribute(Constant.CLASSROOM_OBJECT, new Classroom());
        return Constant.ADDCLASSROOM_VIEW;
    }

    @GetMapping("/listClassroom")
    public String listClassroom(Model model){
        List<Student> studentList = studentService.findAll();
        List<Classroom> classroomList = classroomService.findAll();

        classroomList.forEach(classroom -> {
            studentList.forEach(student -> {
                if (classroom.getId().equals(student.getClassroom().getId()))
                    classroom.setStudentExist(true);
            });
        });

        model.addAttribute(Constant.CLASSROOM_OBJECT_LIST, classroomList);
        return Constant.LISTCLASSROOM_VIEW;
    }

    @PostMapping("/saveClassroom")
    public String saveClassroom(@ModelAttribute("classroom") Classroom classroom, Model model){
        if (classroom.getId() != null){
            Classroom classroomUpdated = classroomService.findById(classroom.getId());
            classroomUpdated.setClassroomName(classroom.getClassroomName());
            classroomUpdated.setAbbreviation(classroom.getAbbreviation());
            classroomService.createClassroom(classroomUpdated);
            model.addAttribute(Constant.CLASSROOM_OBJECT_LIST, classroomService.findAll());
            model.addAttribute(Constant.UPDATE, true);
            return Constant.LISTCLASSROOM_VIEW;

        }else{
            if(classroomService.findByClassroomNameOrAbr(classroom.getClassroomName(),classroom.getAbbreviation()) == null){
                classroomService.createClassroom(classroom);
                model.addAttribute(Constant.SUCCESS, true);
                model.addAttribute(Constant.CLASSROOM_OBJECT, new Classroom());
            }
            else
                model.addAttribute(Constant.ERROR, true);

        }

        return Constant.ADDCLASSROOM_VIEW;
    }

    @GetMapping("/deleteClassroom")
    public String deleteClassroom(@RequestParam("classroomId") Long  classroomId, Model model){
        if (classroomService.findById(classroomId) != null)
            classroomService.deleteClassroom(classroomService.findById(classroomId));


        model.addAttribute(Constant.CLASSROOM_OBJECT_LIST, classroomService.findAll());
        return Constant.LISTCLASSROOM_VIEW;
    }

    @GetMapping("/editClassroom")
    public String editClassroom(@RequestParam("classroomId") Long  classroomId, Model model){
        if (classroomService.findById(classroomId) != null)
            model.addAttribute(Constant.CLASSROOM_OBJECT, classroomService.findById(classroomId));

        return Constant.ADDCLASSROOM_VIEW;
    }
}
