package com.yorosoft.eschool.controller;

import com.yorosoft.eschool.model.Classroom;
import com.yorosoft.eschool.model.Student;
import com.yorosoft.eschool.service.ClassroomService;
import com.yorosoft.eschool.service.StudentService;
import com.yorosoft.eschool.utility.Constant;
import org.apache.tomcat.util.bcel.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class StudentController {

    private final StudentService studentService;
    private final ClassroomService classroomService;

    @Autowired
    public StudentController(StudentService studentService,ClassroomService classroomService) {
        this.studentService = studentService;
        this.classroomService = classroomService;
    }

    @GetMapping("/addStudent")
    public String addStudent(Model model){
        model.addAttribute(Constant.STUDENT_OBJET, new Student());
        model.addAttribute(Constant.CLASSROOM_OBJECT_LIST, classroomService.findAll());
        model.addAttribute("classroomName", "");
        model.addAttribute(Constant.CREATE_MODE, true);
        return Constant.ADD_STUDENT_VIEW;
    }

    @GetMapping("/listStudent")
    public String listClassroom(Model model){
        model.addAttribute(Constant.LIST_STUDENT, studentService.findAll());
        return Constant.LIST_STUDENT_VIEW;
    }

    @PostMapping("/saveStudent")
    public String saveClassroom(@ModelAttribute(Constant.STUDENT_OBJET) Student student,
                                Model model, @ModelAttribute("classroomName") String classroomName){
        List<Student> studentList = studentService.findAllByClassroomClassroomName(classroomName);
        if (studentList.size() == classroomService.findByClassroomName(classroomName).getClassroomStudentsCount()){
            model.addAttribute(Constant.EFFECTIF_ERROR, true);
            model.addAttribute(Constant.CLASSROOM_OBJECT_LIST, classroomService.findAll());
            return Constant.ADD_STUDENT_VIEW;

        }

        if (studentList.size()>0){
            for (int i = 0; i < studentList.size(); i++) {
                if (studentList.get(i).getFname().equals(student.getFname()) && studentList.get(i).getLname().equals(student.getLname())){
                    model.addAttribute(Constant.ERROR, true);
                    model.addAttribute(Constant.CLASSROOM_OBJECT_LIST, classroomService.findAll());
                    return Constant.ADD_STUDENT_VIEW;
                }

            }
        }

        student.setClassroom(classroomService.findByClassroomName(classroomName));
        studentService.createStudent(student);
        model.addAttribute(Constant.SUCCESS,true);
        model.addAttribute(Constant.STUDENT_OBJET, new Student());
        model.addAttribute(Constant.CLASSROOM_OBJECT_LIST, classroomService.findAll());
        return Constant.ADD_STUDENT_VIEW;
    }

    @GetMapping("/deleteStudent")
    public String deleteStudent(@RequestParam("studentId") Long  studentId, Model model){
        Student student = studentService.findById(studentId);
        studentService.deleteStudent(student);
        model.addAttribute(Constant.LIST_STUDENT, studentService.findAll());
        return Constant.LIST_STUDENT_VIEW;
    }

    @GetMapping("/editStudent")
    public String editStudent(@RequestParam("studentId") Long  studentId, Model model){

        if (studentService.findById(studentId) != null){
            model.addAttribute(Constant.EDIT_MODE, true);
            model.addAttribute(Constant.STUDENT_OBJET, studentService.findById(studentId));
        }
        else{
            model.addAttribute(Constant.CREATE_MODE, true);
            model.addAttribute(Constant.STUDENT_OBJET, new Student());

        }

        model.addAttribute(Constant.CLASSROOM_OBJECT_LIST, classroomService.findAll());
        return Constant.ADD_STUDENT_VIEW;
    }
}
