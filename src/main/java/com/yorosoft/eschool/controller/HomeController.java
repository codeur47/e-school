package com.yorosoft.eschool.controller;

import com.yorosoft.eschool.service.ClassroomService;
import com.yorosoft.eschool.utility.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.stream.Collectors;

@Controller
public class HomeController {

    private final ClassroomService classroomService;

    @Autowired
    public HomeController(ClassroomService classroomService) {
        this.classroomService = classroomService;
    }

    @GetMapping("/")
    public String index(Model model){
        model.addAttribute(Constant.CLASSROOM_OBJECT_LIST, classroomService.findAll().stream()
                .limit(5)
                .collect(Collectors.toList()));
        return Constant.DASHBOARD_VIEW;
    }

    @GetMapping("/index")
    public String dashboard(Model model){
        model.addAttribute(Constant.CLASSROOM_OBJECT_LIST, classroomService.findAll());
        return Constant.DASHBOARD_VIEW;
    }

    @GetMapping("/dashboard")
    public String homeDashboard(Model model){
        model.addAttribute(Constant.CLASSROOM_OBJECT_LIST, classroomService.findAll().stream()
                .limit(5)
                .collect(Collectors.toList()));
        return Constant.DASHBOARD_VIEW; }

}
