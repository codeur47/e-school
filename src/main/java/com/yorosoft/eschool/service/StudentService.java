package com.yorosoft.eschool.service;

import com.yorosoft.eschool.model.Classroom;
import com.yorosoft.eschool.model.Student;

import java.util.List;

public interface StudentService {
    void createStudent(Student student);
    List<Student> findAll();
    Student findById(Long id);
    Student findByLnameAndFname(String lname, String fname);
    List<Student> findAllByClassroomId(Long id);
    List<Student>findAllByClassroomClassroomName(String classroomName);
    void deleteStudent(Student student);
}
