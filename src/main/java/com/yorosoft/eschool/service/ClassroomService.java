package com.yorosoft.eschool.service;

import com.yorosoft.eschool.model.Classroom;

import java.util.List;

public interface ClassroomService {
    void createClassroom(Classroom classroom);
    List<Classroom> findAll();
    Classroom findById(Long classroomId);
    void deleteClassroom(Classroom classroom);
    Classroom findByClassroomNameOrAbr(String classroomName, String classroomAbr);
    Classroom findByClassroomName(String classroomName);
}
