package com.yorosoft.eschool.service.impl;

import com.yorosoft.eschool.model.Student;
import com.yorosoft.eschool.repository.StudentRepository;
import com.yorosoft.eschool.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public void createStudent(Student student) {
        studentRepository.save(student);
    }

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student findById(Long id) {
        if (studentRepository.findById(id).isPresent())
            return studentRepository.findById(id).get();
        return null;
    }

    @Override
    public Student findByLnameAndFname(String lname, String fname) {
        return studentRepository.findByLnameAndFname(lname, fname);
    }

    @Override
    public List<Student> findAllByClassroomId(Long id) {
        return studentRepository.findAllByClassroomId(id);
    }

    @Override
    public List<Student> findAllByClassroomClassroomName(String classroomName) {
        return studentRepository.findAllByClassroomClassroomName(classroomName);
    }

    @Override
    public void deleteStudent(Student student) {
        studentRepository.delete(student);
    }
}
