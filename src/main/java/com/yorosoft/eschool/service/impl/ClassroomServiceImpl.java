package com.yorosoft.eschool.service.impl;

import com.yorosoft.eschool.model.Classroom;
import com.yorosoft.eschool.repository.ClassroomRepository;
import com.yorosoft.eschool.service.ClassroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ClassroomServiceImpl implements ClassroomService {

    private final ClassroomRepository classroomRepository;

    @Autowired
    public ClassroomServiceImpl(ClassroomRepository classroomRepository) {
        this.classroomRepository = classroomRepository;
    }

    @Override
    public void createClassroom(Classroom classroom) {
       classroomRepository.save(classroom);
    }

    @Override
    public List<Classroom> findAll() {
        return classroomRepository.findAll();
    }

    @Override
    public Classroom findById(Long classroomId) {
        Optional<Classroom> classroom = classroomRepository.findById(classroomId);
        return classroom.orElse(null);
    }

    @Override
    public void deleteClassroom(Classroom classroom) {
        classroomRepository.delete(classroom);
    }

    @Override
    public Classroom findByClassroomNameOrAbr(String classroomName, String classroomAbr) {
        return this.classroomRepository.findByClassroomNameOrOrAbbreviation(classroomName,classroomAbr);
    }

    @Override
    public Classroom findByClassroomName(String classroomName) {
        return classroomRepository.findByClassroomName(classroomName);
    }

}
