package com.yorosoft.eschool.repository;

import com.yorosoft.eschool.model.Classroom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassroomRepository extends JpaRepository<Classroom, Long> {
    Classroom findByClassroomName(String classroomName);
    Classroom findByAbbreviation(String abbreviation);
    Classroom findByClassroomNameOrOrAbbreviation(String classroomName, String abbreviation);
}
