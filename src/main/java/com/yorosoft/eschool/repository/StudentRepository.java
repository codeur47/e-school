package com.yorosoft.eschool.repository;

import com.yorosoft.eschool.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findByLnameAndFname(String lname, String fname);
    List<Student> findAllByClassroomId(Long id);
    List<Student>findAllByClassroomClassroomName(String classroomName);
}
